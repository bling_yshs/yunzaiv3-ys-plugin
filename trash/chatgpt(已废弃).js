/**
 * chatgpt
 * 1.需要安装依赖pnpm add ora -w和pnpm add chatgpt -w
 * 2.需要自己注册openai(或者说chatgpt)账号(友情提醒：非常麻烦),具体请自行寻找b站教程
 * 3.还需要获取sessionToken,具体请看 https://github.com/transitive-bullshit/chatgpt-api#session-tokens。
 */

var session = "你的sessionToken，双引号不要删除";
var keyword = "";

import { oraPromise } from "ora";
import { ChatGPTAPI } from "chatgpt";

import plugin from "../../lib/plugins/plugin.js";

export class example extends plugin {
    constructor() {
        super({
            name: "关键词禁言",
            dsc: "简单开发示例",
            event: "message",
            priority: 5000,
            rule: [
                {
                    reg: "#chatgpt",
                    fnc: "chatgpt",
                },
            ],
        });
    }
    async chatgpt(e) {
        //获取#chatgpt以后的文字
        let msg = e.message[0].text;
        keyword = msg.split("#chatgpt")[1];
        //如果开头是空格，去掉空格
        if (keyword[0] == " ") {
            keyword = keyword.slice(1);
        }
        const api = new ChatGPTAPI({
            sessionToken: session,
        });
        await api.ensureAuth();

        const prompt = keyword;

        const response = await oraPromise(api.sendMessage(prompt), {
            text: prompt,
        });

        e.reply(response, true);
    }
}
