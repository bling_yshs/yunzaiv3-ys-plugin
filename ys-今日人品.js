﻿/**
 * 介绍：计算今日人品(根据时间和QQ号等概率哈希，所以结果仅供参考)
 * 样例：今日人品
 */

import plugin from '../../lib/plugins/plugin.js'
import fs from 'fs'
import path from 'path'

export class example extends plugin {
  constructor() {
    super({
      name: 'ys-今日人品',
      dsc: 'ys-今日人品',
      event: 'message',
      priority: 5000,
      rule: [{
        reg: '^#?(今日人品|jrrp)$', fnc: 'jrrp'
      }]
    })
  }
  
  async jrrp(e) {
    let qq = e.sender.user_id
    let today = new Date()
    let past = today.getDate() - 1
    today.setDate(past)
    today.setHours(0, 0, 0, 0)
    let seed = (today * qq).toString()
    let randomNumber = this.hashCode(seed)
    let res = Math.abs(this.hashCode(Math.abs(randomNumber).toString())) % 101
    let comment = this.generateComment(res)
    
    const dioDir = './data/ys-dio-pic'
    let dioPicList = fs.readdirSync(dioDir)
    if (dioPicList.length === 0) {
      e.reply([
        segment.at(qq),
        '你今天的人品是：',
        comment,
        '\n提示：沙雕图文件夹为空，请执行以下命令获取图片:\ngit clone --depth=1 https://gitee.com/bling_yshs/ys-dio-pic-repo.git ./data/ys-dio-pic'
      ])
      return
    }
    
    let dioTuPath = path.join(dioDir, dioPicList[Math.floor(Math.random() * dioPicList.length)])
    Bot.logger.info(`发送了一张吊图: ${dioTuPath}`)
    e.reply([
      segment.at(qq),
      '你今天的人品是：',
      comment,
      segment.image(dioTuPath)
    ])
  }
  
  generateComment(num) {
    let comment = ''
    Bot.logger.info(`今日人品：${num}, ${typeof num}`)
    switch (true) {
      case (num === 0):
        comment = `${num}！！！非到极致也是欧！`
        break
      case (num === 100):
        comment = `${num}！${num}！${num}！！！难道这就是传说中的欧皇吗 d(ŐдŐ๑)`
        break
      case (num === 99):
        comment = `${num}！居然就差一点点么！`
        break
      case (num === 50):
        comment = `${num}！五五开......`
        break
      case (num >= 80):
        comment = `${num}，好评如潮！`
        break
      case (num >= 60):
        comment = `${num}，今天运气不错呢！`
        break
      case (num >= 50):
        comment = `${num}，不错呦！`
        break
      case (num >= 20):
        comment = `${num}，没关系，看张吊图缓一缓 ◝(⑅•ᴗ•⑅)◜..°♡`
        break
      case (num >= 10):
        comment = `${num}？！不会吧......`
        break
      case (num >= 1):
        comment = `${num}......（是百分制哦）`
        break
      default:
        comment = 'Invalid number'
    }
    return comment
  }
  
  hashCode(str) {
    let hash = 0
    if (str.length === 0) return hash
    for (let i = 0; i < str.length; i++) {
      const char = str.charCodeAt(i)
      hash = (hash << 5) - hash + char
      hash &= hash
    }
    return hash
  }
}
