# yunzaiv3-ys-plugin

## 介绍

一些云崽 v3 的插件，很多都写的不够完善 （能用的会写在下面 ↓)

### 可用的插件

#### 米游社cos

- 介绍：发送随机米游社cos图片

- 触发：`cos`

#### 今日人品

- 介绍：计算今日人品(根据时间和QQ号等概率哈希，所以结果仅供参考)

- 触发：`今日人品`

#### 沙雕图插件

- 介绍：随机发送一张沙雕图，图库在 [https://gitee.com/bling_yshs/ys-dio-pic-repo](https://gitee.com/bling_yshs/ys-dio-pic-repo)  
下载后放入 `yunzai-bot/data/ys-dio-pic` 下，也就是会得到 `yunzai-bot/data/ys-dio-pic/abc.image`

- 触发：`吊图`

#### 俄罗斯轮盘插件

- 从 [憨憨插件](https://gitee.com/han-hanz/hanhan-plugin)
  独立出来重写的插件

- 触发：`开枪`

- 效果图:
  <br>
  <img src='res/俄罗斯轮盘效果图.png' width="50%">
  <br>

#### woc 插件

- 从 [闲心插件](https://gitee.com/xianxincoder/xianxin-plugin)
  独立出来的插件，可以发送一些攒劲图片。相较于原版，拥有更简洁的语法，并适配 `Miao-Yunzai`

- 触发：`woc` 或者 `#woc`

#### ChatGPT

- 样例 `q4鲁迅为什么暴打周树人`

- 备注：
  - 依赖库 `pnpm install openai -w`
  - 回复AI的回答，可以连续对话
  - 需要购买API-KEY

#### Github 仓库卡片生存

- 触发：当检测到Github仓库链接，例如 `https://github.com/icqqjs/icqq`

- 效果图:
  <br>
  <img src='res/github仓库卡片效果图.png' width="50%">
  <br>

#### 能不能好好说话

- 发送拼音缩写可以得到原本的意思，效果同 [https://lab.magiconch.com/nbnhhsh](https://lab.magiconch.com/nbnhhsh)

- 触发：`#hhsh yyds`

#### 精致睡眠

- 很简单的插件，发送精致睡眠可以自动禁言 8 小时，js 内可以自定义回复内容和禁言时间

#### 留言插件

- 留一条消息给群友，当他下次说话时，机器人会自动发送留言

- 触发：`留言@真寻 明天帮我打深渊`
